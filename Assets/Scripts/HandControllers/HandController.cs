﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour
{
	[SerializeField] GameObject HandMotion;

	bool isConnected;
	private SerialConnectionController serialConnectionController;
	private MotionHandController motionHandController;
	private InterfaceController interfaceController;

	private float scale;
	private int before;
	private int beforeGrab = 3300;
	// Start is called before the first frame update
	void Start()
	{
		Debug.Log("Hand Controller Working ...");
		serialConnectionController = GetComponent<SerialConnectionController>();
		motionHandController = GetComponent<MotionHandController>();
		interfaceController = GetComponent<InterfaceController>();
		serialConnectionController.InitializeDevice("COM3", 9600);
		serialConnectionController.StartDeviceLink();
	}

	// Update is called once per frame
	void Update()
	{
		if(!SerialConnectionController.SCInstance.isOpen){
			Debug.Log("Sin Conexion ....");
			serialConnectionController.StartDeviceLink();
		}else{
			ArrayList DataFetch = interfaceController.DefineData();  
			foreach (var Value in DataFetch) {
				var Movement = (Movement) Value;
				Debug.Log(Movement.value);
				if(Movement.type == "HAX"){
					motionHandController.HandSupiPronMotion(HandMotion, Movement.value/-2);
				}else if(Movement.type == "HAY") {
					motionHandController.HandFlexStretchMotion(HandMotion, -Movement.value/2);
				}
			}
		}
		if(Input.GetKey(KeyCode.RightArrow)){
			motionHandController.HandSupiPronMotion(HandMotion, -1);
		}
		if(Input.GetKey(KeyCode.LeftArrow)){
			motionHandController.HandSupiPronMotion(HandMotion, 1);
		}
		if(Input.GetKey(KeyCode.UpArrow)){
			motionHandController.HandFlexStretchMotion(HandMotion, 1);
		}
		if(Input.GetKey(KeyCode.DownArrow)){
			motionHandController.HandFlexStretchMotion(HandMotion, -1);
		}
		if(Input.GetKey(KeyCode.RightControl)){
			motionHandController.HandPositionMotion(HandMotion, -1);
		}
	}
}
