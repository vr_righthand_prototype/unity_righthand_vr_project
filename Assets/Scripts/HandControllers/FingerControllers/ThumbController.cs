﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThumbController : MonoBehaviour
{
	[SerializeField] GameObject ThumbBone1Motion;
	[SerializeField] GameObject ThumbBone2Motion;
	
	private InterfaceController interfaceController;
	private MotionGrabController motionGrabController;
	private MotionExtendController motionExtendController;
	private SerialConnectionController serialConnectionController;

	private float scale;
	private int beforeIMR;
	private int beforeMRR;

	private int beforeGrab = 3300;
	// Start is called before the first frame update

	void Awake () {
		scale = 1f;
		interfaceController = GetComponent<InterfaceController>();
		motionGrabController = GetComponent<MotionGrabController>();
		motionExtendController = GetComponent<MotionExtendController>();
		serialConnectionController = GetComponent<SerialConnectionController>();

	}
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		if(SerialConnectionController.SCInstance.isOpen){
			ArrayList DataFetch = interfaceController.DefineData();  
			foreach (var Value in DataFetch) {
				var Movement = (Movement) Value;
				if(Movement.type == "IFR"){
					int Move = (beforeGrab - Movement.value) / 10;
					motionGrabController.UpdateThumbMotion(ThumbBone1Motion, -Move);
					motionGrabController.UpdateThumbMotion(ThumbBone2Motion, -Move);
					beforeGrab = Movement.value;
				}
			}
		} else {
			if (Input.GetMouseButton(0)){
				motionExtendController.UpdateBoneMotion(ThumbBone1Motion, -3);
			}
			if (Input.GetMouseButton(1)){
				motionExtendController.UpdateBoneMotion(ThumbBone1Motion, 3);
			}
			// Blend-Unblend Bone 1 Motion
			motionGrabController.UpdateBoneMotion(ThumbBone1Motion, Mathf.RoundToInt(Input.mouseScrollDelta.y));
			// Grab Hand Bone 2-3 Motion
			if(Input.GetKey(KeyCode.G)){
				motionGrabController.UpdateThumbMotion(ThumbBone2Motion, -2);
			}
			// Ungrab Hand Bone 2-3 Motion
			if(Input.GetKey(KeyCode.B)){
				motionGrabController.UpdateThumbMotion(ThumbBone2Motion, 2);
			}
		}
	}
}
