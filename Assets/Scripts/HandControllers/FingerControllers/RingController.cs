﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingController : MonoBehaviour
{
	[SerializeField] GameObject RingBone1Motion;
	[SerializeField] GameObject RingBone2Motion;
	[SerializeField] GameObject RingBone3Motion;
	private InterfaceController interfaceController;
	private MotionGrabController motionGrabController;
	private MotionExtendController motionExtendController;
	private SerialConnectionController serialConnectionController;

	private float scale;
	private int beforeMRR;
	private int beforeRPR;

	private int beforeGrab = 3300;

	void Awake () {
		scale = 1f;
		interfaceController = GetComponent<InterfaceController>();
		motionGrabController = GetComponent<MotionGrabController>();
		motionExtendController = GetComponent<MotionExtendController>();
		serialConnectionController = GetComponent<SerialConnectionController>();

	}
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		if(SerialConnectionController.SCInstance.isOpen){
			ArrayList DataFetch = interfaceController.DefineData();  
			foreach (var Value in DataFetch) {
				var Movement = (Movement) Value;
				if(Movement.type == "MfR"){
					if(Movement.value == 0 && beforeMRR != Movement.value){
						motionExtendController.UpdateBoneMotion(RingBone1Motion, 20);
					} else if(Movement.value == 1 && beforeMRR != Movement.value) {
						motionExtendController.UpdateBoneMotion(RingBone1Motion, -20);
					}
					beforeMRR = Movement.value;
				}
				if(Movement.type == "RfR"){
					if(Movement.value == 0 && beforeRPR != Movement.value){
						motionExtendController.UpdateBoneMotion(RingBone1Motion, -20);
					} else if(Movement.value == 1 && beforeRPR != Movement.value) {
						motionExtendController.UpdateBoneMotion(RingBone1Motion, 20);
					}
					beforeRPR = Movement.value;
				}
				if(Movement.type == "RFR"){
					int Move = (beforeGrab - Movement.value) / 10;
					motionGrabController.UpdateBoneMotion(RingBone1Motion, Move);
					motionGrabController.UpdateBoneMotion(RingBone2Motion, Move);
					motionGrabController.UpdateDependantBoneMotion(RingBone3Motion, Move/2);
					beforeGrab = Movement.value;
				}
			}
		} else { 
			if (Input.GetMouseButtonDown(0)){
				motionExtendController.UpdateBoneMotion(RingBone1Motion, 10);
			}
			if (Input.GetMouseButtonDown(1)){
				motionExtendController.UpdateBoneMotion(RingBone1Motion, -10);
			}
			// Blend-Unblend Bone 1 Motion
			motionGrabController.UpdateBoneMotion(RingBone1Motion, Mathf.RoundToInt(Input.mouseScrollDelta.y));
			// Grab Hand Bone 2-3 Motion
			if(Input.GetKey(KeyCode.E)){
				motionGrabController.UpdateBoneMotion(RingBone2Motion, -2);
				motionGrabController.UpdateDependantBoneMotion(RingBone3Motion, -1);
			}
			// Ungrab Hand Bone 2-3 Motion
			if(Input.GetKey(KeyCode.D)){
				motionGrabController.UpdateBoneMotion(RingBone2Motion, 2);
				motionGrabController.UpdateDependantBoneMotion(RingBone3Motion, 1);
			}
		}
	}
}
