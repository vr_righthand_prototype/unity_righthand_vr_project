﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkyController : MonoBehaviour
{
	[SerializeField] GameObject PinkyBone1Motion;
	[SerializeField] GameObject PinkyBone2Motion;
	[SerializeField] GameObject PinkyBone3Motion;
	
	private InterfaceController interfaceController;
	private MotionGrabController motionGrabController;
	private MotionExtendController motionExtendController;
	private SerialConnectionController serialConnectionController;
	private float scale;
	private int before;
	private int beforeGrab = 3300;
	// Start is called before the first frame update

	void Awake () {
		scale = 1f;
		interfaceController = GetComponent<InterfaceController>();
		motionGrabController = GetComponent<MotionGrabController>();
		motionExtendController = GetComponent<MotionExtendController>();
		serialConnectionController = GetComponent<SerialConnectionController>();

	}
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		if(SerialConnectionController.SCInstance.isOpen){
			ArrayList DataFetch = interfaceController.DefineData();  
			foreach (var Value in DataFetch) {
				var Movement = (Movement) Value;
				if(Movement.type == "RfR"){
					if(Movement.value == 0 && before != Movement.value){
						motionExtendController.UpdateBoneMotion(PinkyBone1Motion, -20);
					} else if(Movement.value == 1 && before != Movement.value) {
						motionExtendController.UpdateBoneMotion(PinkyBone1Motion, 20);
					}
					before = Movement.value;
				}
				if(Movement.type == "PFR"){
					int Move = (beforeGrab - Movement.value) / 10;
					motionGrabController.UpdateBoneMotion(PinkyBone1Motion, Move);
					motionGrabController.UpdateBoneMotion(PinkyBone2Motion, Move);
					motionGrabController.UpdateDependantBoneMotion(PinkyBone3Motion, Move/2);
					beforeGrab = Movement.value;
				}
			}
		} else { 
			if (Input.GetMouseButtonDown(0)){
				motionExtendController.UpdateBoneMotion(PinkyBone1Motion, 20);
			}
			if (Input.GetMouseButtonDown(1)){
				motionExtendController.UpdateBoneMotion(PinkyBone1Motion, -20);
			}
			// Blend-Unblend Bone 1 Motion
			motionGrabController.UpdateBoneMotion(PinkyBone1Motion, Mathf.RoundToInt(Input.mouseScrollDelta.y));
			// Grab Hand Bone 2-3 Motion
			if(Input.GetKey(KeyCode.R)){
				motionGrabController.UpdateBoneMotion(PinkyBone2Motion, -2);
				motionGrabController.UpdateDependantBoneMotion(PinkyBone3Motion, -1);
			}
			// Ungrab Hand Bone 2-3 Motion
			if(Input.GetKey(KeyCode.F)){
				motionGrabController.UpdateBoneMotion(PinkyBone2Motion, 2);
				motionGrabController.UpdateDependantBoneMotion(PinkyBone3Motion, 1);
			}
		}
	}
}
