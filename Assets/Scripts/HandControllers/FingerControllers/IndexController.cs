﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndexController : MonoBehaviour
{
	[SerializeField] GameObject IndexBone1Motion;
	[SerializeField] GameObject IndexBone2Motion;
	[SerializeField] GameObject IndexBone3Motion;
	[SerializeField] string [] Values;
	
	private InterfaceController interfaceController;
	private MotionGrabController motionGrabController;
	private MotionExtendController motionExtendController;
	private float scale;
	private int before;
	private int beforeGrab = 3300;
	// Start is called before the first frame update

	void Awake () {
		scale = 1f;
		interfaceController = GetComponent<InterfaceController>();
		motionGrabController = GetComponent<MotionGrabController>();
		motionExtendController = GetComponent<MotionExtendController>();
		motionExtendController.UpdateBoneMotion(IndexBone1Motion, -20);
	}
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		if(SerialConnectionController.SCInstance.isOpen){
			ArrayList DataFetch = interfaceController.DefineData(); 
			foreach (var Value in DataFetch) {
				var Movement = (Movement) Value;
				if(Movement.type == "IfR"){
					if(Movement.value == 0 && before != Movement.value){
						motionExtendController.UpdateBoneMotion(IndexBone1Motion, -20);
					} else if(Movement.value == 1 && before != Movement.value) {
						motionExtendController.UpdateBoneMotion(IndexBone1Motion, 20);
					}
					before = Movement.value;
				}
				if(Movement.type == "IFR"){
					int Move = (beforeGrab - Movement.value) / 10;
					motionGrabController.UpdateBoneMotion(IndexBone1Motion, Move);
					motionGrabController.UpdateBoneMotion(IndexBone2Motion, Move);
					motionGrabController.UpdateDependantBoneMotion(IndexBone3Motion, Move);
					beforeGrab = Movement.value;
				}
			}
		} else {
			if (Input.GetMouseButtonDown(0)){
				motionExtendController.UpdateBoneMotion(IndexBone1Motion, -20);
			}
			if (Input.GetMouseButtonDown(1)){
				motionExtendController.UpdateBoneMotion(IndexBone1Motion, 20);
			}
			// Blend-Unblend Bone 1 Motion
			motionGrabController.UpdateBoneMotion(IndexBone1Motion, Mathf.RoundToInt(Input.mouseScrollDelta.y));
			// Grab Hand Bone 2-3 Motion
			if(Input.GetKey(KeyCode.Q)){
				motionGrabController.UpdateBoneMotion(IndexBone2Motion, -2);
				motionGrabController.UpdateDependantBoneMotion(IndexBone3Motion, -1);
			}
			// Ungrab Hand Bone 2-3 Motion
			if(Input.GetKey(KeyCode.A)){
				motionGrabController.UpdateBoneMotion(IndexBone2Motion, 2);
				motionGrabController.UpdateDependantBoneMotion(IndexBone3Motion, 1);
			}
		}
	}
}
