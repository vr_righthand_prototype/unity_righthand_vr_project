﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiddleController : MonoBehaviour
{
	[SerializeField] GameObject MiddleBone1Motion;
	[SerializeField] GameObject MiddleBone2Motion;
	[SerializeField] GameObject MiddleBone3Motion;
	
	private InterfaceController interfaceController;
	private MotionGrabController motionGrabController;
	private MotionExtendController motionExtendController;
	private SerialConnectionController serialConnectionController;

	private float scale;
	private int beforeIMR;
	private int beforeMRR;

	private int beforeGrab = 3300;
	// Start is called before the first frame update

	void Awake () {
		scale = 1f;
		interfaceController = GetComponent<InterfaceController>();
		motionGrabController = GetComponent<MotionGrabController>();
		motionExtendController = GetComponent<MotionExtendController>();
		serialConnectionController = GetComponent<SerialConnectionController>();

	}
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		if(SerialConnectionController.SCInstance.isOpen){
			ArrayList DataFetch = interfaceController.DefineData(); 
			foreach (var Value in DataFetch) {
				var Movement = (Movement) Value;
				if(Movement.type == "IfR"){
					if(Movement.value == 0 && beforeIMR != Movement.value){
						motionExtendController.UpdateBoneMotion(MiddleBone1Motion, 20);
					} else if(Movement.value == 1 && beforeIMR != Movement.value) {
						motionExtendController.UpdateBoneMotion(MiddleBone1Motion, -20);
					}
					beforeIMR = Movement.value;
				}
				if(Movement.type == "MfR"){
					if(Movement.value == 0 && beforeMRR != Movement.value){
						motionExtendController.UpdateBoneMotion(MiddleBone1Motion, -20);
					} else if(Movement.value == 1 && beforeMRR != Movement.value) {
						motionExtendController.UpdateBoneMotion(MiddleBone1Motion, 20);
					}
					beforeMRR = Movement.value;
				}
				if(Movement.type == "MFR"){
					int Move = (beforeGrab - Movement.value) / 10;
					motionGrabController.UpdateBoneMotion(MiddleBone1Motion, Move);
					motionGrabController.UpdateBoneMotion(MiddleBone2Motion, Move);
					motionGrabController.UpdateDependantBoneMotion(MiddleBone3Motion, Move/2);
					beforeGrab = Movement.value;
				}
			}
		} else { 
			if (Input.GetMouseButtonDown(0)){
				motionExtendController.UpdateBoneMotion(MiddleBone1Motion, -2);
			}
			if (Input.GetMouseButtonDown(1)){
				motionExtendController.UpdateBoneMotion(MiddleBone1Motion, 2);
			}
			// Blend-Unblend Bone 1 Motion
			motionGrabController.UpdateBoneMotion(MiddleBone1Motion, Mathf.RoundToInt(Input.mouseScrollDelta.y));

			// Grab Hand Bone 2-3 Motion
			if(Input.GetKey(KeyCode.W)){
				motionGrabController.UpdateBoneMotion(MiddleBone2Motion, -2);
				motionGrabController.UpdateDependantBoneMotion(MiddleBone3Motion, -1);
			}

			// Ungrab Hand Bone 2-3 Motion
			if(Input.GetKey(KeyCode.S)){
				motionGrabController.UpdateBoneMotion(MiddleBone2Motion, 2);
				motionGrabController.UpdateDependantBoneMotion(MiddleBone3Motion, 1);
			}
		}
	}
}
