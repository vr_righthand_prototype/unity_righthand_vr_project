﻿using System.Collections.Generic;
using System.Collections;
using System.IO.Ports;
using UnityEngine;

public class SerialConnectionController : MonoBehaviour
{
	SerialPort serialPort = new SerialPort();

	public string[] SplitData;
	public bool isOpen;
	public static SerialConnectionController SCInstance;
	// Start is called before the first frame update

	void Awake(){
    SCInstance = this;
 	}

	void Update () {
		isOpen = serialPort.IsOpen;
		if(serialPort.IsOpen){
			receiveData();
		}
	}
	public void InitializeDevice(string portName, int baudRate) {
		serialPort.PortName = portName;
		serialPort.BaudRate = baudRate;
	}
	public void StartDeviceLink() {
		try {
			serialPort.Open();
		}
		catch (System.Exception ex) {
			Debug.Log(ex);
		}
	}

	void receiveData() {
		try {
			SplitData = serialPort.ReadLine().Split(',');
		} catch (System.TimeoutException) {
			SplitData = new string [0];
		}
	}
}
