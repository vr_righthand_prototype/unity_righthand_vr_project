﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement {


    public string type;
    public int value;


}
public class InterfaceController : MonoBehaviour
{
	public ArrayList DefineData() {
		ArrayList Data = new ArrayList();
		if(SerialConnectionController.SCInstance.SplitData.Length > 0){
			for(int i=0; i < SerialConnectionController.SCInstance.SplitData.Length;i++) {
				Movement movement = new Movement ();
				string [] Values = SerialConnectionController.SCInstance.SplitData[i].Split('=');
				if(Values.Length == 2){
					Debug.Log(Values[1]);
					movement.value = int.Parse(Values[1]);
					movement.type = Values[0];
					Data.Add(movement);
				}
			}
		}
		return Data;
	}
}
