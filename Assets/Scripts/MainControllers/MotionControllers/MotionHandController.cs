﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionHandController : MonoBehaviour
{
    float speed = 0.5f;
    private float t;
    public void HandSupiPronMotion(GameObject BoneMotion, int yAxis) {
		BoneMotion.transform.Rotate(0, yAxis, 0);
	}

    // Start is called before the first frame update
    public void HandFlexStretchMotion(GameObject BoneMotion, int xAxis) {
		BoneMotion.transform.Rotate(xAxis, 0, 0);
	}

    public void HandPositionMotion(GameObject BoneMotion, int yAxis) {
        t += Time.deltaTime * speed;
		transform.position = transform.position + new Vector3(0, yAxis, 0);
	}
}
