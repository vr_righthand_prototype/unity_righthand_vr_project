﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionGrabController : MonoBehaviour
{
	public void UpdateBoneMotion(GameObject BoneMotion, int xAxis) {
		BoneMotion.transform.Rotate(xAxis, 0, 0);
	}

	public void UpdateThumbMotion(GameObject BoneMotion, int zAxis) {
		BoneMotion.transform.Rotate(0, 0, zAxis);
	}
	public void UpdateDependantBoneMotion(GameObject BoneMotion, int xAxis) {
		BoneMotion.transform.Rotate(xAxis, 0, 0);
	}
}
