﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionExtendController : MonoBehaviour
{
  public void UpdateBoneMotion(GameObject BoneMotion, int zAxis) {
		BoneMotion.transform.Rotate(0, 0, zAxis);
	}
}
